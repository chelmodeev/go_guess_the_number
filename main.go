package main

import (
	"bufio"
	"fmt"
	"log"
	"math/rand"
	"os"
	"strings"
	"time"
)

func main() {
	var complexity int

	rand.Seed(time.Now().UTC().UnixNano())
	randomInteger := rand.Intn(100)

	fmt.Println("Я хочу сыграть с тобой в одну игру.")
	fmt.Println("Тебе надо угадать число за определенное количество попыток")
	fmt.Println("Выбери сложность: easy, medium, hard")

	inputComplexity := bufio.NewReader(os.Stdin)
	inputComplexityValue, err := inputComplexity.ReadString('\n')

	if err != nil {
		log.Fatal(err)
	}

	inputComplexityValue = strings.TrimSpace(inputComplexityValue)

	if inputComplexityValue == "easy" {
		complexity = 6
	} else if inputComplexityValue == "medium" {
		complexity = 5
	} else if inputComplexityValue == "hard" {
		complexity = 4
	} else {
		fmt.Println("Выбери корректное значение")
	}

	attempts := complexity

	fmt.Println(complexity)
	fmt.Println(randomInteger)

	fmt.Println("Я загадал число. У тебя осталось", attempts, "попыток")
	fmt.Println("Попробуй угадать число")
	inputComplexity = bufio.NewReader(os.Stdin)
	inputComplexityValue, err = inputComplexity.ReadString('\n')

	if err != nil {
		log.Fatal(err)
	}
}
